def Fizzbuzz(turn):
    returnValue = ""

    if turn % 3 == 0:
        returnValue = "Fizz"
    if turn % 5 == 0:
        returnValue += "Buzz"
    
    if not returnValue: 
        returnValue = turn

    return returnValue