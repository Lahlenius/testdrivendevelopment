import pytest
from fizzbuzz import Fizzbuzz 

def test_WHEN_FizzbuzzIsCalled_THEN_FizzbuzzReturns():
    return Fizzbuzz(1)

def test_GIVEN_1_THEN_FizzbuzzReturns1():
    returnValue = Fizzbuzz(1)
    assert returnValue == 1

def test_GIVEN_2_THEN_FizzbuzzReturns2():
    returnValue = Fizzbuzz(2)
    assert returnValue == 2

def test_GIVEN_3_THEN_FizzbuzzReturnsFizz():
    returnValue = Fizzbuzz(3)
    assert returnValue == "Fizz"

def test_GIVEN_4_THEN_FizzbuzzReturns4():
    returnValue = Fizzbuzz(4)
    assert returnValue == 4

def test_GIVEN_5_THEN_FizzbuzzReturnsBuzz():
    returnValue = Fizzbuzz(5)
    assert returnValue == "Buzz"

def test_GIVEN_NumberDivisibleBy3_THEN_FizzbuzzReturnsFizz():
    returnValue = Fizzbuzz(333)
    assert returnValue == "Fizz"

def test_GIVEN_NumberDivisibleBy5_THEN_FizzbuzzReturnsBuzz():
    returnValue = Fizzbuzz(55)
    assert returnValue == "Buzz"

def test_GIVEN_NumberDivisibleBy3And5_THEN_FizzbuzzReturnsFizzBuzz():
    returnValue = Fizzbuzz(15)
    assert returnValue == "FizzBuzz"